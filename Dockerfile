FROM openjdk:14-buster
ARG container_userid

RUN apt-get update && \
    apt-get install -y --no-install-recommends \ 
       maven \
       && \
    rm -rf /var/lib/apt/lists/*

RUN groupadd user
RUN useradd --shell /bin/bash -u ${container_userid} -o -c "" -m user -g user

USER user
ENV buildroot=/home/user/build
RUN mkdir -p ${buildroot}
ADD --chown=user:user https://gitlab.developers.cam.ac.uk/api/v4/projects/794/repository/branches ${buildroot}/java-accessor-version.json
RUN git clone https://gitlab.developers.cam.ac.uk/cst/dtg/automated-teaching/java-accessor.git ${buildroot}/java-accessor
WORKDIR ${buildroot}/java-accessor
RUN git checkout $(git for-each-ref refs/tags --sort=-taggerdate --format='%(refname)' --count=1)
RUN mvn clean compile package install

ADD --chown=user:user https://gitlab.developers.cam.ac.uk/api/v4/projects/795/repository/branches ${buildroot}/task-template.json
RUN git clone https://gitlab.developers.cam.ac.uk/cst/dtg/automated-teaching/oop-task-template.git ${buildroot}/task-template
WORKDIR ${buildroot}/task-template/test/java
RUN mvn -B dependency:go-offline clean compile test package
WORKDIR ${buildroot}/task-template/skeleton/java
RUN mvn -B dependency:go-offline clean compile test package
RUN mvn -B org.apache.maven.plugins:maven-dependency-plugin:3.1.1:get  -Dartifact=org.apache.maven.plugins:maven-release-plugin:2.3.2

WORKDIR /home/user
RUN rm -rf ${buildroot}

USER root
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN mkdir /pottery-binaries
COPY bin/* /pottery-binaries/
RUN chmod 755 /usr/local/bin/entrypoint.sh /pottery-binaries/*


USER user
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
