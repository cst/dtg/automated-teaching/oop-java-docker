#!/bin/bash

TASKDIR=$1

cd $TASKDIR/test/java
MAVEN_OPTS="-XX:MaxRAM=$(( $RAM_LIMIT_MEGABYTES - 80))m --add-opens java.base/java.lang=ALL-UNNAMED --add-opens jdk.compiler/com.sun.tools.javac.model=ALL-UNNAMED --add-opens jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED" mvn -q -B clean compile package
mkdir -p $TASKDIR/common
cp target/*-with-dependencies.jar $TASKDIR/common/test-harness.jar
