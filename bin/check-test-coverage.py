#!/usr/bin/python

import csv
import sys
import os
import re

def exec_jacoco(submissiondir):
    current = os.getcwd()
    os.chdir(submissiondir)
    retcode = os.system("MAVEN_OPTS=\"-XX:MaxRAM=$(( $RAM_LIMIT_MEGABYTES - 80))m --add-opens java.base/java.lang=ALL-UNNAMED --add-opens jdk.compiler/com.sun.tools.javac.model=ALL-UNNAMED --add-opens jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED\" mvn -B -o jacoco:report")
    if retcode != 0:
        sys.exit(-1)
    os.chdir(current)

def process_csv(filename, requiredCoveragePercent):

    totalMissed = 0
    totalCovered = 0

    print("")
    print("")
    print("Coverage report")
    print("")

    if not os.path.exists(filename):
        print("Test coverage results not found - check that tests exist and are being executed")
        sys.exit(-1)

    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        for row in list(reader)[1:]:
            package = row[1]
            clazz = row[2]
            if re.match(r'Main.*$',clazz):
                continue
            instructionsMissed = float(row[3])
            instructionsCovered = float(row[4])
            print("%-50s%d%%" % (package + "." + clazz, instructionsCovered / (instructionsMissed + instructionsCovered) * 100))
            totalMissed += instructionsMissed
            totalCovered += instructionsCovered

    overall = totalCovered / (totalMissed + totalCovered) * 100
    print("--")
    print("%-50s%d%%" % ("OVERALL",overall))
    print("--")
    print("%-50s%d%%" % ("REQUIRED",requiredCoveragePercent))
    return overall



if __name__ == "__main__":
    (submissiondir,requiredCoveragePercent) = (sys.argv[1],int(sys.argv[2]))
    exec_jacoco(submissiondir)
    overall = process_csv("%s/target/site/jacoco/jacoco.csv" % submissiondir, requiredCoveragePercent)
    if overall < requiredCoveragePercent:
        sys.exit(-1)
    else:
        sys.exit(0)
