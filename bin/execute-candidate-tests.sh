#!/bin/bash

SUBMISSIONDIR=$1

cd $SUBMISSIONDIR
MAVEN_OPTS="-XX:MaxRAM=$(( $RAM_LIMIT_MEGABYTES - 80))m --add-opens java.base/java.lang=ALL-UNNAMED --add-opens jdk.compiler/com.sun.tools.javac.model=ALL-UNNAMED --add-opens jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED" mvn -B -o test
