#!/bin/bash

SOLUTIONDIR=$1
FILE_TO_CHECK=$2

echo "Checking that $FILE_TO_CHECK does not contain TODO messages"
awk '/TODO/{exit -1}' $SOLUTIONDIR/$FILE_TO_CHECK


