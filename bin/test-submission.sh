#!/bin/bash

HARNESSDIR=$1
SOLUTIONDIR=$2
TESTMAIN=$3

if compgen -G "$HARNESSDIR/*.jar" >/dev/null 2>/dev/null; then
    echo "Test harness binary found"
else
    echo "No test harness binary found"
    exit -1
fi

if find $SOLUTIONDIR/target/classes -name "*.class" >/dev/null 2>/dev/null; then
    echo "Compiled submission found"
else
    echo "No submitted program found"
    exit -1
fi

java \
    -XX:MaxRAM=$(( $RAM_LIMIT_MEGABYTES -80))m \
    -cp $HARNESSDIR/*:$SOLUTIONDIR/target/classes \
    $TESTMAIN



